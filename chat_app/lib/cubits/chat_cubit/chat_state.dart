// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'chat_cubit.dart';

class ChatState {}

class ChatInitial extends ChatState {}

class ChatSuccess extends ChatState {
  List<Message> messagesList;
  ChatSuccess({required this.messagesList});
}

class ChatFaillure extends ChatState {
  final String errMessage;

  ChatFaillure({required this.errMessage});
}
