part of 'auth_cubit.dart';

class AuthState {}

class AuthInitial extends AuthState {}

class LoginLoading extends AuthState {}

class LoginSuccess extends AuthState {}

class LoginFailure extends AuthState {
  final String errMessage;
  LoginFailure({required this.errMessage});
}

class RegisterLoading extends AuthState {}

class RegisterSucess extends AuthState {}

class RegisterFailure extends AuthState {
  final String errMessage;
  RegisterFailure({required this.errMessage});
}
