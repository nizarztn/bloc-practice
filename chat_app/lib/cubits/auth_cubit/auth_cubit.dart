import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  String? email;
  String? password;

  Future<void> loginUser({required String email, required String password}) async {
    emit(LoginLoading());
    try {
      try {
        await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
      } on FirebaseAuthException catch (ex) {
        if (ex.code == 'user-not-found') {
          emit(LoginFailure(errMessage: 'user-not-found'));
        } else if (ex.code == 'wrong-password') {
          emit(LoginFailure(errMessage: 'wrong-password'));
        }
      } catch (ex) {
        emit(LoginFailure(errMessage: 'Exception: ${ex.toString()}'));
      }
      emit(LoginSuccess());
    } on Exception catch (e) {
      emit(LoginFailure(errMessage: 'Somthing went wrong: ${e.toString()}'));
    }
  }

  Future<void> registerUser({required String email, required String password}) async {
    emit(RegisterLoading());
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (ex) {
      if (ex.code == 'weak-password') {
        emit(RegisterFailure(errMessage: 'weak password'));
      } else if (ex.code == 'email-already-in-use') {
        emit(RegisterFailure(errMessage: 'email-already-in-use'));
      }
    } catch (ex) {
      emit(RegisterFailure(errMessage: 'Exception: ${ex.toString()}'));
    }
  }
}
