part of 'tenis_counter_cubit.dart';

@immutable
abstract class TenisCounterState {}

class TenisCounterInitial extends TenisCounterState {}

class CounterIncrementState extends TenisCounterState {}
