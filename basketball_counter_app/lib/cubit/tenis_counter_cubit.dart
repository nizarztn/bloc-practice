import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'tenis_counter_state.dart';

class TenisCounterCubit extends Cubit<TenisCounterState> {
  TenisCounterCubit() : super(TenisCounterInitial());
  int counterA = 0;
  int counterB = 0;

  void counterIncrement(int incValue, String aOrB) {
    if (aOrB == 'A') {
      counterA += incValue;
      emit(CounterIncrementState());
    } else {
      counterB += incValue;
      emit(CounterIncrementState());
    }
  }
}
