import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubit/tenis_counter_cubit.dart';

void main() {
  runApp(PointsCounterState());
}

class PointsCounterState extends StatelessWidget {
  const PointsCounterState({Key? key}) : super(key: key);

  // int teamAPoints = 0;
  // int teamBPoints = 0;

  // void addOnePoint() {
  //   print('add one point');
  // }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TenisCounterCubit(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: BlocConsumer<TenisCounterCubit, TenisCounterState>(
          listener: (context, state) {
            //
          },
          builder: (context, state) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.orange,
                title: Text('Points Counter'),
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 500,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'Team A',
                              style: TextStyle(
                                fontSize: 32,
                              ),
                            ),
                            Text(
                              '${BlocProvider.of<TenisCounterCubit>(context).counterA}',
                              style: TextStyle(
                                fontSize: 32,
                              ),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(8),
                                backgroundColor: Colors.orange,
                                minimumSize: Size(150, 50),
                              ),
                              onPressed: () {
                                BlocProvider.of<TenisCounterCubit>(context).counterIncrement(1, 'A');
                              },
                              child: Text(
                                'Add 1 Point ',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.orange,
                                minimumSize: Size(150, 50),
                              ),
                              onPressed: () {
                                BlocProvider.of<TenisCounterCubit>(context).counterIncrement(2, 'A');
                              },
                              child: Text(
                                'Add 2 Point',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.orange,
                                minimumSize: Size(150, 50),
                              ),
                              onPressed: () {
                                BlocProvider.of<TenisCounterCubit>(context).counterIncrement(3, 'A');
                              },
                              child: Text(
                                'Add 3 Point ',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 500,
                        child: VerticalDivider(
                          indent: 50,
                          endIndent: 50,
                          color: Colors.grey,
                          thickness: 1,
                        ),
                      ),
                      SizedBox(
                        height: 500,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'Team B',
                              style: TextStyle(
                                fontSize: 32,
                              ),
                            ),
                            Text(
                              '${BlocProvider.of<TenisCounterCubit>(context).counterB}',
                              style: TextStyle(
                                fontSize: 32,
                              ),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(8),
                                backgroundColor: Colors.orange,
                                minimumSize: Size(150, 50),
                              ),
                              onPressed: () {
                                BlocProvider.of<TenisCounterCubit>(context).counterIncrement(1, 'B');
                              },
                              child: Text(
                                'Add 1 Point ',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.orange,
                                minimumSize: Size(150, 50),
                              ),
                              onPressed: () {
                                BlocProvider.of<TenisCounterCubit>(context).counterIncrement(2, 'B');
                              },
                              child: Text(
                                'Add 2 Point ',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.orange,
                                minimumSize: Size(150, 50),
                              ),
                              onPressed: () {
                                BlocProvider.of<TenisCounterCubit>(context).counterIncrement(3, 'B');
                              },
                              child: Text(
                                'Add 3 Point ',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(8),
                      backgroundColor: Colors.orange,
                      minimumSize: Size(150, 50),
                    ),
                    onPressed: () {
                      BlocProvider.of<TenisCounterCubit>(context).counterA = 0;
                      BlocProvider.of<TenisCounterCubit>(context).counterB = 0;
                    },
                    child: Text(
                      'Reset',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
